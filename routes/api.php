<?php

use App\Http\Controllers\PersonApiController;
use App\Http\Controllers\ShipOrderApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/person', [PersonApiController::class, 'getAll']);
Route::get('/person/{id}', [PersonApiController::class, 'find']);
Route::get('/order', [ShipOrderApiController::class, 'getAll']);
Route::get('/order/{id}', [ShipOrderApiController::class, 'getAll']);
