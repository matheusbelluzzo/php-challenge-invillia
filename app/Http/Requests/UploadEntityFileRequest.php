<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadEntityFileRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'entityFile' => 'required|file'
        ];
    }
}
