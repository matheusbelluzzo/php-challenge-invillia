<?php


namespace App\Http\Controllers;


use Domain\Customer\Service\PersonService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

final class PersonApiController extends Controller
{
    private PersonService $personService;

    public function __construct(PersonService $personService)
    {
        $this->personService = $personService;
    }

    public function getAll(): JsonResponse
    {
        $people = $this->personService->getAll();

        return response()->json([
            'status' => Response::HTTP_OK,
            'data' => [
                'people' => $people
            ]
        ]);
    }

    public function find(int $id): JsonResponse
    {
        try {
            return response()
                ->json([
                    'status' => Response::HTTP_OK,
                    'data' => [
                        'person' => $this->personService->find($id)
                    ]
                ]);
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json([
                    'status' => Response::HTTP_NOT_FOUND,
                    'message' => 'Person not found for the given id'
                ], Response::HTTP_NOT_FOUND);
        }
    }
}
