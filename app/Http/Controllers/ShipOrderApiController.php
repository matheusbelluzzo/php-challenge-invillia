<?php


namespace App\Http\Controllers;

use Domain\Order\Service\OrderService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

final class ShipOrderApiController extends Controller
{
    private OrderService $orderService;

    public function __construct(OrderService $personService)
    {
        $this->orderService = $personService;
    }

    public function getAll(): JsonResponse
    {
        $shipOrders = $this->orderService->getAll();

        return response()->json([
            'status' => Response::HTTP_OK,
            'data' => [
                'shipOrders' => $shipOrders
            ]
        ]);
    }

    public function find(int $id): JsonResponse
    {
        try {
            return response()
                ->json([
                    'status' => Response::HTTP_OK,
                    'data' => [
                        'shipOrders' => $this->orderService->find($id)
                    ]
                ]);
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json([
                    'status' => Response::HTTP_NOT_FOUND,
                    'message' => $exception->getMessage()
                ], Response::HTTP_NOT_FOUND);
        }
    }
}
