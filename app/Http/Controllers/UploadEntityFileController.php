<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadEntityFileRequest;
use Domain\Support\Exceptions\FileAlreadyImportedException;
use Domain\Support\Service\EntityXmlFileHandlerService;

class UploadEntityFileController extends Controller
{
    private EntityXmlFileHandlerService $entityXmlFileHandler;

    public function __construct(EntityXmlFileHandlerService $entityXmlFileHandler)
    {
        $this->entityXmlFileHandler = $entityXmlFileHandler;
    }

    public function getUploadEntityFilePage()
    {
        return view('index');
    }

    public function processEntityFile(UploadEntityFileRequest $request)
    {
        $file = $request->file('entityFile');

        try {
            $this->entityXmlFileHandler->handle($file->path());
        } catch (FileAlreadyImportedException $exception) {
            return view('index', ['error_message' => $exception->getMessage()]);
        }

        return view('index', ['success_message' => 'The file has been successfully imported!']);
    }
}
