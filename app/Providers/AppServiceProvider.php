<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Domain\Customer\Persistence\PersonEloquentRepository;
use Domain\Customer\Persistence\PersonRepositoryInterface;
use Domain\Customer\Persistence\PhoneEloquentRepository;
use Domain\Customer\Persistence\PhoneRepositoryInterface;
use Domain\Order\Persistence\OrderEloquentRepository;
use Domain\Order\Persistence\OrderItemEloquentRepository;
use Domain\Order\Persistence\OrderItemRepositoryInterface;
use Domain\Order\Persistence\OrderRepositoryInterface;
use Domain\Order\Persistence\OrderShipAddressEloquentRepository;
use Domain\Order\Persistence\OrderShipAddressRepositoryInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(OrderRepositoryInterface::class, OrderEloquentRepository::class);
        $this->app->bind(OrderItemRepositoryInterface::class, OrderItemEloquentRepository::class);
        $this->app->bind(OrderShipAddressRepositoryInterface::class, OrderShipAddressEloquentRepository::class);

        $this->app->bind(PersonRepositoryInterface::class, PersonEloquentRepository::class);
        $this->app->bind(PhoneRepositoryInterface::class, PhoneEloquentRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
