<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>Your Website</title>
    </head>
    <body>
    <div class="container-sm">
        <h1>PHP Challenge Invillia</h1>
        @if ($errors->any())
            <div>
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (isset($success_message))
            <div class="alert alert-success">
                <p>{{ $success_message }}</p>
            </div>
        @endif
        @if (isset($error_message))
            <div>
                <p>{{ $error_message }}</p>
            </div>
        @endif

        <form action="" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                @csrf
                <label for="entityFile">Xml File</label>
                <input type="file" class="form-control-file" id="entityFile" name="entityFile">
                <br>
                <div>
                    <input type="submit" value="Process" class="btn btn-primary">
                </div>
            </div>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>
