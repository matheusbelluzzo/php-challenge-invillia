<?php


namespace Domain\Customer\Service;


use Domain\Customer\Entity\Phone;
use Domain\Customer\Persistence\PhoneRepositoryInterface;

final class PhoneService
{
    private PhoneRepositoryInterface $phoneRepository;

    public function __construct(PhoneRepositoryInterface $phoneRepository)
    {
        $this->phoneRepository = $phoneRepository;
    }

    public function save(Phone $phone)
    {
        $this->phoneRepository->save($phone);
    }

    public function findByNumber(string $number): Phone
    {
        return $this->phoneRepository->findOrFailByNumber($number);
    }

    public function update(Phone $phone): void
    {
        $this->phoneRepository->update($phone);
    }
}
