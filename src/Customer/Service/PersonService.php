<?php


namespace Domain\Customer\Service;


use Domain\Customer\Entity\Person;
use Domain\Customer\Persistence\PersonRepositoryInterface;

final class PersonService
{
    private PersonRepositoryInterface $personRepository;

    public function __construct(PersonRepositoryInterface $personRepository)
    {
        $this->personRepository = $personRepository;
    }

    public function save(Person $person)
    {
        $this->personRepository->save($person);
    }

    public function getAll(): array
    {
        return $this->personRepository->getAllWithRelations();
    }

    public function find(int $id): Person
    {
        return $this->personRepository->findOrFail($id);
    }

    public function update(Person $person): void
    {
        $this->personRepository->update($person);
    }
}
