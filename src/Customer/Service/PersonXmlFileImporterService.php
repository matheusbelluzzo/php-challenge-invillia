<?php

namespace Domain\Customer\Service;

use Domain\Customer\Entity\Person;
use Domain\Customer\Entity\Phone;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use SimpleXMLElement;

final class PersonXmlFileImporterService
{
    private PersonService $personService;
    private PhoneService $phoneService;

    public function __construct(PersonService $personService, PhoneService $phoneService)
    {
        $this->personService = $personService;
        $this->phoneService = $phoneService;
    }

    public function import(SimpleXMLElement $xml): void
    {
        foreach ($xml as $element) {
            $person = $this->importPerson($element);
            $this->importPhone($element, $person);
        }
    }

    private function importPerson(SimpleXMLElement $element): Person
    {
        try {
            $person = $this->personService->find((int) $element->personid);
            $person->setName($element->personname);
            $this->personService->update($person);
        } catch(ModelNotFoundException $exception) {
            $person = new Person($element->personname, (int) $element->personid);
            $this->personService->save($person);
        }

        return $person;
    }

    private function importPhone(SimpleXMLElement $element, Person $person): void
    {
        foreach ($element->phones->phone as $phoneNumber) {
            try {
                $phone = $this->phoneService->findByNumber($phoneNumber);
                $phone->setPersonId($person->getId());
                $this->phoneService->update($phone);
            } catch (ModelNotFoundException $exception) {
                $phone = new Phone($phoneNumber, $person->getId());
                $this->phoneService->save($phone);
            }
        }
    }
}
