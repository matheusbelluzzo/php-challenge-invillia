<?php

namespace Domain\Customer\Entity;

final class PhoneArray
{
    public function __invoke(Phone $phone): array
    {
        return [
            'id' => $phone->getId(),
            'personId' => $phone->getPersonId(),
            'number' => $phone->getNumber()
        ];
    }
}
