<?php

namespace Domain\Customer\Entity;

class PersonArray
{
    public function __invoke(Person $person)
    {
        return [
            'id' => $person->getId(),
            'name' => $person->getName()
        ];
    }
}
