<?php

namespace Domain\Customer\Entity;

final class Phone
{
    private ?int $id;
    private string $number;
    private int $personId;

    public function __construct(string $number, int $personId, ?int $id = null)
    {
        $this->number = $number;
        $this->personId = $personId;
        $this->id = $id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    public function setPersonId(int $personId): void
    {
        $this->personId = $personId;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getPersonId(): int
    {
        return $this->personId;
    }
}
