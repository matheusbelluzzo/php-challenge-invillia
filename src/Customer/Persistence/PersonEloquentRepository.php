<?php


namespace Domain\Customer\Persistence;

use Domain\Customer\Entity\Person;

final class PersonEloquentRepository implements PersonRepositoryInterface
{
    public function save(Person $person): void
    {
        $personEloquent = PersonEloquent::createFromEntity($person);
        $personEloquent->save();
        $person->setId($personEloquent->id);
    }

    public function getAllWithRelations(): array
    {
        /** @var PersonEloquentCollection $people */
        $people = PersonEloquent::with(['phones'])
            ->get();
        return $people->toEntityArray();
    }

    public function findOrFail(int $id): Person
    {
        /** @var PersonEloquent $person */
        $person = PersonEloquent::findOrFail($id);
        return $person->toEntity();
    }

    public function update(Person $person): void
    {
        $personEloquent = PersonEloquent::createFromEntity($person);
        $personEloquent->exists = true;
        $personEloquent->update();
    }
}
