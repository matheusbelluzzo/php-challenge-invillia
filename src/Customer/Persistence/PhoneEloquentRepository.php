<?php


namespace Domain\Customer\Persistence;

use Domain\Customer\Entity\Phone;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PhoneEloquentRepository implements PhoneRepositoryInterface
{
    public function save(Phone $phone): void
    {
        $phoneEloquent = PhoneEloquent::createFromEntity($phone);
        $phoneEloquent->save();
        $phone->setId($phoneEloquent->id);
    }

    public function findOrFailByNumber(string $number): Phone
    {
        $phoneCollection = PhoneEloquent::where('number', '=', $number)->get();

        if ($phoneCollection->isEmpty()) {
            throw new ModelNotFoundException();
        }

        return $phoneCollection->first()->toEntity();
    }

    public function update(Phone $phone): void
    {
        $phoneEloquent = PhoneEloquent::createFromEntity($phone);
        $phoneEloquent->exists = true;
        $phoneEloquent->update();
    }
}
