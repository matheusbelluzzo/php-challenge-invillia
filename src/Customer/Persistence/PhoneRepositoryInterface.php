<?php


namespace Domain\Customer\Persistence;

use Domain\Customer\Entity\Phone;

interface PhoneRepositoryInterface
{
    public function save(Phone $phone): void;

    public function findOrFailByNumber(string $number): Phone;

    public function update(Phone $phone): void;
}
