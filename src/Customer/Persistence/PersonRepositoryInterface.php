<?php


namespace Domain\Customer\Persistence;

use Domain\Customer\Entity\Person;

interface PersonRepositoryInterface
{
    public function save(Person $person): void;

    public function getAllWithRelations(): array;

    public function findOrFail(int $id): Person;

    public function update(Person $person): void;
}
