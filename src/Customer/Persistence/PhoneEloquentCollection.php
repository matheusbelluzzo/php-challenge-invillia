<?php


namespace Domain\Customer\Persistence;


use Domain\Customer\Entity\PhoneArray;
use Illuminate\Database\Eloquent\Collection;

class PhoneEloquentCollection extends Collection
{
    public function toEntityArray(): array
    {
        return $this->reduce(function (array $phoneCollection, PhoneEloquent $phone) {
            $phoneArray = new PhoneArray();
            $phoneCollection[] = $phoneArray($phone->toEntity());
            return $phoneCollection;
        }, []);
    }
}
