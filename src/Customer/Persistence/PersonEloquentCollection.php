<?php


namespace Domain\Customer\Persistence;

use Domain\Customer\Entity\PersonArray;
use Illuminate\Support\Collection;

final class PersonEloquentCollection extends Collection
{
    public function toEntityArray(): array
    {
        return $this->reduce(function (array $personCollection, PersonEloquent $personEloquent) {
            $phones = $this->getPhonesArrayFromPerson($personEloquent);
            $person = $this->getPersonArray($personEloquent);
            $personCollection[] = array_merge($person, $phones);
            return $personCollection;
        }, []);
    }

    private function getPhonesArrayFromPerson(PersonEloquent $personEloquent): array
    {
        return [
            'phones' => $personEloquent->phones->toEntityArray()
        ];
    }

    private function getPersonArray(PersonEloquent $personEloquent): array
    {
        $personArray = new PersonArray;
        return $personArray($personEloquent->toEntity());
    }
}
