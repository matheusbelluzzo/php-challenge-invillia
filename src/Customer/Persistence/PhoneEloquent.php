<?php


namespace Domain\Customer\Persistence;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Domain\Customer\Entity\Phone;

class PhoneEloquent extends Model
{
    use HasFactory;

    protected $table = 'phones';

    protected $fillable = [
        'number', 'person_id'
    ];

    public static function createFromEntity(Phone $phone): self
    {
        $phoneEloquent = new self();
        $phoneEloquent->id = $phone->getId();
        $phoneEloquent->number = $phone->getNumber();
        $phoneEloquent->person_id = $phone->getPersonId();

        return $phoneEloquent;
    }

    public function toEntity(): Phone
    {
        return new Phone($this->number, $this->person_id, $this->id);
    }

    public function newCollection(array $models = [])
    {
        return new PhoneEloquentCollection($models);
    }
}
