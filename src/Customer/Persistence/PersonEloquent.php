<?php


namespace Domain\Customer\Persistence;

use Domain\Customer\Entity\Person;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

final class PersonEloquent extends Model
{
    use HasFactory;

    protected $table = 'people';

    protected $fillable = [
        'name'
    ];

    public function phones()
    {
        return $this->hasMany(PhoneEloquent::class, 'person_id', 'id');
    }

    public static function createFromEntity(Person $person): self
    {
        $personEloquent = new self();
        $personEloquent->id = $person->getId();
        $personEloquent->name = $person->getName();

        return $personEloquent;
    }

    public function toEntity(): Person
    {
        return new Person($this->name, $this->id);
    }

    public function newCollection(array $models = [])
    {
        return new PersonEloquentCollection($models);
    }
}
