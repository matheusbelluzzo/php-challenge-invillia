<?php


namespace Domain\Order\Persistence;


use Domain\Order\Entity\Order;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use InvalidArgumentException;

class OrderEloquentRepository implements OrderRepositoryInterface
{
    public function save(Order $order): void
    {
        $orderEloquent = OrderEloquent::createFromEntity($order);
        $orderEloquent->save();
        $order->setId($orderEloquent->id);
    }

    public function getAll(): array
    {
        /** @var OrderEloquentCollection $orders */
        $orders = OrderEloquent::with(['orderShipAddress', 'orderItems'])
            ->get();
        return $orders->toEntityArray();
    }

    public function findOrFail(int $id): Order
    {
        /** @var OrderEloquent $person */
        $order = OrderEloquent::findOrFail($id);
        return $order->toEntity();
    }
}
