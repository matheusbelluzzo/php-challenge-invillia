<?php


namespace Domain\Order\Persistence;


use Domain\Order\Entity\OrderItem;

interface OrderItemRepositoryInterface
{
    public function save(OrderItem $orderItem): void;
}
