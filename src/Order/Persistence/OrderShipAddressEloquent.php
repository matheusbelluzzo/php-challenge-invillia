<?php


namespace Domain\Order\Persistence;


use Illuminate\Database\Eloquent\Model;
use Domain\Order\Entity\OrderShipAddress;

class OrderShipAddressEloquent extends Model
{
    protected $table = 'orders_ship_addresses';

    protected $fillable = [
        'name',
        'address',
        'city',
        'country',
    ];

    public static function createFromEntity(OrderShipAddress $orderShipAddress): self
    {
        return new self([
            'name' => $orderShipAddress->getName(),
            'address' => $orderShipAddress->getAddress(),
            'city' => $orderShipAddress->getCity(),
            'country' => $orderShipAddress->getCountry(),
        ]);
    }

    public function toEntity(): OrderShipAddress
    {
        return new OrderShipAddress($this->name, $this->address, $this->city, $this->country, $this->id);
    }

    public function newCollection(array $models = [])
    {
        return new OrderShipAddressEloquentCollection($models);
    }
}
