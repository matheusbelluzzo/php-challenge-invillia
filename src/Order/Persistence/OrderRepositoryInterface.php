<?php


namespace Domain\Order\Persistence;


use Domain\Order\Entity\Order;

interface OrderRepositoryInterface
{
    public function save(Order $oder): void;

    public function getAll(): array;

    public function findOrFail(int $id): Order;
}
