<?php


namespace Domain\Order\Persistence;

use Domain\Order\Entity\OrderItem;

class OrderItemEloquentRepository implements OrderItemRepositoryInterface
{
    public function save(OrderItem $orderItem): void
    {
        $orderEloquent = OrderItemEloquent::createFromEntity($orderItem);
        $orderEloquent->save();
        $orderItem->setId($orderEloquent->id);
    }
}
