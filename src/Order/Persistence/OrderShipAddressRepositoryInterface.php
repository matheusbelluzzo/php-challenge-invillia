<?php


namespace Domain\Order\Persistence;


use Domain\Order\Entity\OrderShipAddress;

interface OrderShipAddressRepositoryInterface
{
    public function save(OrderShipAddress $orderShipAddress): void;
}
