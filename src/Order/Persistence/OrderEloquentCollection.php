<?php


namespace Domain\Order\Persistence;


use Domain\Order\Entity\OrderArray;
use Domain\Order\Entity\OrderShipAddressArray;
use Illuminate\Database\Eloquent\Collection;

class OrderEloquentCollection extends Collection
{
    public function toEntityArray(): array
    {
        return $this->reduce(function (array $orderCollection, OrderEloquent $orderEloquent) {
            $order = $this->getOrderArray($orderEloquent);
            $orderShipAddress = $this->getOrderShipAddressArrayFromOrder($orderEloquent);
            $orderItems = $this->getOrderItemsArrayFromOrder($orderEloquent);
            $orderCollection[] = array_merge($order, $orderShipAddress, $orderItems);
            return $orderCollection;
        }, []);
    }

    private function getOrderShipAddressArrayFromOrder(OrderEloquent $order): array
    {
        $orderShipAddress = $order->orderShipAddress->toEntity();
        $orderShipAddressArray = new OrderShipAddressArray();

        return [
            'orderShipAddress' => $orderShipAddressArray($orderShipAddress)
        ];
    }

    private function getOrderItemsArrayFromOrder(OrderEloquent $order): array
    {
        return [
            'items' => $order->orderItems->toEntityArray()
        ];
    }

    private function getOrderArray(OrderEloquent $order): array
    {
        $orderArray = new OrderArray();
        return $orderArray($order->toEntity());
    }
}
