<?php


namespace Domain\Order\Persistence;


use Domain\Order\Entity\OrderItemArray;
use Illuminate\Database\Eloquent\Collection;

class OrderItemEloquentCollection extends Collection
{
    public function toEntityArray(): array
    {
        return $this->reduce(function (array $orderItemCollection, OrderItemEloquent $orderItem) {
            $orderItemArray = new OrderItemArray();
            $orderItemCollection[] = $orderItemArray($orderItem->toEntity());
            return $orderItemCollection;
        }, []);
    }
}
