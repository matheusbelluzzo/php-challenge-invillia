<?php


namespace Domain\Order\Persistence;


use Illuminate\Database\Eloquent\Model;
use Domain\Order\Entity\OrderItem;

class OrderItemEloquent extends Model
{
    protected $table = 'orders_items';

    protected $fillable = [
        'order_id',
        'title',
        'note',
        'quantity',
        'price',
    ];

    public static function createFromEntity(OrderItem $order): self
    {
        return new self([
            'order_id' => $order->getOrderId(),
            'title' => $order->getTitle(),
            'note' => $order->getNote(),
            'quantity' => $order->getQuantity(),
            'price' => $order->getPrice(),
        ]);
    }

    public function toEntity(): OrderItem
    {
        return new OrderItem($this->order_id, $this->title, $this->note, $this->quantity, $this->price, $this->id);
    }

    public function newCollection(array $models = [])
    {
        return new OrderItemEloquentCollection($models);
    }
}
