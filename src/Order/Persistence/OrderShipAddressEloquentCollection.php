<?php


namespace Domain\Order\Persistence;


use Domain\Order\Entity\OrderShipAddressArray;
use Illuminate\Database\Eloquent\Collection;

class OrderShipAddressEloquentCollection extends Collection
{
    public function toEntityArray(): array
    {
        return $this->reduce(function (array $orderShipAddressCollection, OrderShipAddressEloquent $orderShipAddress) {
            $orderShipAddressArray = new OrderShipAddressArray();
            $orderShipAddressCollection[] = $orderShipAddressArray($orderShipAddress->toEntity());
            return $orderShipAddressCollection;
        }, []);
    }
}
