<?php


namespace Domain\Order\Persistence;


use Domain\Order\Entity\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class OrderEloquent extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'person_id',
        'order_ship_address_id'
    ];

    public static function createFromEntity(Order $order): self
    {
        $orderEloquent = new self();
        $orderEloquent->id = $order->getId();
        $orderEloquent->person_id = $order->getPersonId();
        $orderEloquent->order_ship_address_id = $order->getOrderShipAddressId();

        return $orderEloquent;
    }

    public function toEntity(): Order
    {
        return new Order($this->person_id, $this->order_ship_address_id, $this->id);
    }

    public function orderItems(): HasMany
    {
        return $this->hasMany(OrderItemEloquent::class, 'order_id', 'id');
    }

    public function orderShipAddress(): BelongsTo
    {
        return $this->belongsTo(OrderShipAddressEloquent::class, 'order_ship_address_id', 'id');
    }

    public function newCollection(array $models = [])
    {
        return new OrderEloquentCollection($models);
    }
}
