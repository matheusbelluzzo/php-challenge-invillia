<?php


namespace Domain\Order\Persistence;

use Domain\Order\Entity\OrderShipAddress;

class OrderShipAddressEloquentRepository implements OrderShipAddressRepositoryInterface
{
    public function save(OrderShipAddress $orderShipAddress): void
    {
        $orderEloquent = OrderShipAddressEloquent::createFromEntity($orderShipAddress);
        $orderEloquent->save();
        $orderShipAddress->setId($orderEloquent->id);
    }
}
