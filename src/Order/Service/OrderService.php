<?php


namespace Domain\Order\Service;


use Domain\Order\Entity\Order;
use Domain\Order\Persistence\OrderRepositoryInterface;

final class OrderService
{
    private OrderRepositoryInterface $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function save(Order $order): void
    {
        $this->orderRepository->save($order);
    }

    public function getAll(): array
    {
        return $this->orderRepository->getAll();
    }

    public function find(int $id): Order
    {
        return $this->orderRepository->findOrFail($id);
    }
}
