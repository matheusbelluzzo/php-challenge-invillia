<?php

namespace Domain\Order\Service;

use Domain\Order\Entity\Order;
use Domain\Order\Entity\OrderItem;
use Domain\Order\Entity\OrderShipAddress;
use Domain\Support\Exceptions\FileAlreadyImportedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use SimpleXMLElement;

final class ShipOrderXmlFileImporter
{
    private OrderService $orderService;
    private OrderItemService $orderItemService;
    private OrderShipAddressService $orderShipAddressService;

    public function __construct(
        OrderService $orderService,
        OrderItemService $orderItemService,
        OrderShipAddressService $orderShipAddressService
    ) {
        $this->orderService = $orderService;
        $this->orderItemService = $orderItemService;
        $this->orderShipAddressService = $orderShipAddressService;
    }

    public function import(SimpleXMLElement $elements): void
    {
        foreach ($elements as $element) {
            $this->checkIfContentIsAlreadyImported($element);
            $orderShipAddress = $this->importOrderShipAddress($element);
            $order = $this->importOrder($element, $orderShipAddress);
            $this->importOrderItems($element, $order);
        }
    }

    private function importOrderShipAddress(SimpleXMLElement $element): OrderShipAddress
    {
        $orderShipAddress = new OrderShipAddress(
            $element->shipto->name,
            $element->shipto->address,
            $element->shipto->city,
            $element->shipto->country
        );
        $this->orderShipAddressService->save($orderShipAddress);

        return $orderShipAddress;
    }

    private function importOrder(SimpleXMLElement $element, OrderShipAddress $orderShipAddress): Order
    {
        $order = new Order((int) $element->orderperson, $orderShipAddress->getId(), (int) $element->orderid);
        $this->orderService->save($order);

        return $order;
    }

    private function importOrderItems(SimpleXMLElement $element, Order $order): void
    {
        foreach ($element->items->item as $item) {
            $this->importOrderItem($item, $order);
        }
    }

    private function importOrderItem(SimpleXMLElement $item, Order $order): void
    {
        $orderItem = new OrderItem(
            $order->getId(),
            $item->title,
            $item->note,
            (int) $item->quantity,
            (float) $item->price
        );
        $this->orderItemService->save($orderItem);
    }

    private function checkIfContentIsAlreadyImported(SimpleXMLElement $element): void
    {
        try {
            $this->orderService->find((int) $element->orderid);
            throw new FileAlreadyImportedException();
        } catch (ModelNotFoundException $exception) {
            return;
        }
    }
}
