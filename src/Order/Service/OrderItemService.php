<?php


namespace Domain\Order\Service;

use Domain\Order\Entity\OrderItem;
use Domain\Order\Persistence\OrderItemRepositoryInterface;

final class OrderItemService
{
    private OrderItemRepositoryInterface $orderItemRepository;

    public function __construct(OrderItemRepositoryInterface $orderItemRepository)
    {
        $this->orderItemRepository = $orderItemRepository;
    }

    public function save(OrderItem $orderItem): void
    {
        $this->orderItemRepository->save($orderItem);
    }
}
