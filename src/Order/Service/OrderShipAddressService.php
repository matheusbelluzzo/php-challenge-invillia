<?php


namespace Domain\Order\Service;

use Domain\Order\Entity\OrderShipAddress;
use Domain\Order\Persistence\OrderShipAddressRepositoryInterface;

final class OrderShipAddressService
{
    private OrderShipAddressRepositoryInterface $orderShipAddressRepository;

    public function __construct(OrderShipAddressRepositoryInterface $orderShipAddressRepository)
    {
        $this->orderShipAddressRepository = $orderShipAddressRepository;
    }

    public function save(OrderShipAddress $orderShipAddress): void
    {
        $this->orderShipAddressRepository->save($orderShipAddress);
    }
}
