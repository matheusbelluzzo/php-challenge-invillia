<?php


namespace Domain\Order\Entity;


class OrderItemArray
{
    public function __invoke(OrderItem $orderItem): array
    {
        return [
            'id' => $orderItem->getId(),
            'orderId' => $orderItem->getOrderId(),
            'price' => $orderItem->getPrice(),
            'quantity' => $orderItem->getQuantity(),
            'title' => $orderItem->getTitle(),
            'note' => $orderItem->getNote()
        ];
    }
}
