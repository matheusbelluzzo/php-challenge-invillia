<?php


namespace Domain\Order\Entity;


class OrderItem
{
    private ?int $id;
    private int $orderId;
    private string $title;
    private string $note;
    private int $quantity;
    private float $price;

    public function __construct(int $orderId, string $title, string $note, int $quantity, float $price, ?int $id = null)
    {
        $this->orderId = $orderId;
        $this->title = $title;
        $this->note = $note;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->id = $id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getPrice(): float
    {
        return $this->price;
    }
}
