<?php


namespace Domain\Order\Entity;


class OrderShipAddressArray
{
    public function __invoke(OrderShipAddress $orderShipAddress): array
    {
        return [
            'id' => $orderShipAddress->getId(),
            'name' => $orderShipAddress->getName(),
            'address' => $orderShipAddress->getAddress(),
            'city' => $orderShipAddress->getCity(),
            'country' => $orderShipAddress->getCountry()
        ];
    }
}
