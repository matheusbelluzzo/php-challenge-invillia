<?php


namespace Domain\Order\Entity;


class OrderShipAddress
{
    private ?int $id;
    private string $name;
    private string $address;
    private string $city;
    private string $country;

    public function __construct(string $name, string $address, string $city, string $country, ?int $id = null)
    {
        $this->name = $name;
        $this->address = $address;
        $this->city = $city;
        $this->country = $country;
        $this->id = $id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getCountry(): string
    {
        return $this->country;
    }
}
