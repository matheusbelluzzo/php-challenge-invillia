<?php


namespace Domain\Order\Entity;


class OrderArray
{
    public function __invoke(Order $order): array
    {
        return [
            'id' => $order->getId(),
            'orderShipAddressId' => $order->getOrderShipAddressId(),
            'personId' => $order->getPersonId()
        ];
    }
}
