<?php


namespace Domain\Order\Entity;


class Order
{
    private ?int $id;
    private int $personId;
    private int $orderShipAddressId;

    public function __construct(int $personId, int $orderShipAddressId, ?int $id = null)
    {
        $this->id = $id;
        $this->personId = $personId;
        $this->orderShipAddressId = $orderShipAddressId;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderShipAddressId(): int
    {
        return $this->orderShipAddressId;
    }

    public function getPersonId(): int
    {
        return $this->personId;
    }
}
