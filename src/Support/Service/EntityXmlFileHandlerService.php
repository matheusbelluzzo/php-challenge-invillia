<?php


namespace Domain\Support\Service;

use Domain\Customer\Service\PersonXmlFileImporterService;
use Domain\Order\Service\ShipOrderXmlFileImporter;
use SimpleXMLElement;

final class EntityXmlFileHandlerService
{
    private const PERSON_PARSER_OPTION = 'people';
    private const SHIP_ORDER_PARSER_OPTION = 'shiporders';

    private SimpleXMLElement $xml;
    private PersonXmlFileImporterService $personXmlFileImporter;
    private ShipOrderXmlFileImporter $shipOrderXmlFileImporter;

    public function __construct(
        PersonXmlFileImporterService $personXmlFileImporter,
        ShipOrderXmlFileImporter $shipOrderXmlFileImporter
    ) {
        $this->personXmlFileImporter = $personXmlFileImporter;
        $this->shipOrderXmlFileImporter = $shipOrderXmlFileImporter;
    }

    public function handle(string $filePath)
    {
        $this->loadFileXml($filePath);

        if ($this->isPersonXml()) {
            $this->personXmlFileImporter->import($this->xml);
        }

        if ($this->isShipOrderXml()) {
            $this->shipOrderXmlFileImporter->import($this->xml);
        }
    }

    private function loadFileXml($filePath): void
    {
        $content = file_get_contents($filePath);
        $this->xml = simplexml_load_string($content);
    }

    private function isPersonXml(): bool
    {
        return $this->xml->getName() === self::PERSON_PARSER_OPTION;
    }

    private function isShipOrderXml(): bool
    {
        return $this->xml->getName() === self::SHIP_ORDER_PARSER_OPTION;
    }
}
