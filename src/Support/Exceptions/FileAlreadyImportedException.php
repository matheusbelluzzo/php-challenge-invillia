<?php


namespace Domain\Support\Exceptions;


use Exception;
use Throwable;

class FileAlreadyImportedException extends Exception
{
    public function __construct($message = "This file contains already imported elements", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
