# Configurando o projeto

O projeto está configurado para ser utilizado com o docker-compose.

1 - Faça o clone do projeto em sua máquina.

2- Crie o arquivo .env e configure os dados do banco de dados e do usuário
que será utilizado na imagem do docker.

3 - Faça o build da imagem com o docker-compose

`docker-compose build`

4 - Execute os containers

`docker-compose up`

5- Instale as dependências com o composer.

`docker-compose exec app composer install`

6 - Execute as migrations

`docker-compose exec app php artisan migrate
